<?php include("components/header.php");

require_once("source/validation/InputCheck.php");
require_once("source/validation/FormField.php");

use Source\Validation\InputCheck;
use Source\Validation\FormField;

$validation = (new InputCheck($_POST))->validateForm();

if (isset($_POST['type']) && empty($validation)) {
  include("add.php");
}

?>

<head>
  <title>Product Add</title>
</head>
<header class="w-screen max-w-95 border-b py-5 pt-12 mx-auto">
  <div class="flex justify-between">
    <div class="text-4xl">Product Add</div>
    <div class="flex justify-between">
      <div class="mr-12"><button class="text-xl bg-blue-600 py-1 px-3 text-white shadow-btn" id="submit" form="product_form">Save</button></div>
      <div><button class="text-xl bg-red-600 py-1 px-3 text-white shadow-btn"><a href="index.php">Cancel</a></button></div>
    </div>
  </div>
</header>
<main class="w-screen max-w-95 mx-auto">

  <form class="flex flex-col items-start justify-around my-5 text-2xl" id="product_form" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">

    <div class="flex">
      <div class=" w my-2 flex justify-between">
        <label for="sku">SKU</label>
        <input class="border rounded" id="sku" type="text" <?php if (!empty($_POST['sku']) && !empty($validation)) {
                                                              echo "value=\"{$_POST['sku']}\"";
                                                            } ?> name="sku">
      </div>
      <?php if (array_key_exists('sku', $validation)) {
        echo "<div class=\" px-5 \">{$validation['sku']}</div>";
      } ?>
    </div>

    <div class="flex">
      <div class="w my-2 flex justify-between">
        <label for="name">Name</label>
        <input class="border rounded" id="name" type="text" <?php if (!empty($_POST['name']) && !empty($validation)) {
                                                              echo "value=\"{$_POST['name']}\"";
                                                            } ?> name="name">
      </div>
      <?php if (array_key_exists('name', $validation)) {
        echo "<div class=\" px-5 \">{$validation['name']}</div>";
      } ?>
    </div>

    <div class="flex">
      <div class="w my-2 flex justify-between">
        <label for="price">Price ($)</label>
        <input class="border rounded" id="price" type="text" <?php if (!empty($_POST['price']) && !empty($validation)) {
                                                                echo "value=\"{$_POST['price']}\"";
                                                              } ?> name="price">
      </div>
      <?php if (array_key_exists('price', $validation)) {
        echo "<div class=\"px-5\">{$validation['price']}</div>";
      } ?>
    </div>

    <?php

      (new FormField)->typeSwitch($_POST['type'], $validation)

    ?>

  </form>

  <script src="source/script/script.js"></script>

  <?php include("components/footer.php"); ?>
