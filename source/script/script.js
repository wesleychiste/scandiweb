const elements = {
  type: document.querySelector('#productType'),
  dvd: document.querySelector('#DVD'),
  furniture: document.querySelector('#Furniture'),
  book: document.querySelector('#Book'),
  submit: document.querySelector('#submit'),
  form: document.querySelector('#product_form'),
  sku: document.querySelector('#sku'),
  name: document.querySelector('#name'),
  price: document.querySelector('#price'),
  size: document.querySelector('#size'),
  dimensions: {
    h: document.querySelector('#height'),
    w: document.querySelector('#width'),
    l: document.querySelector('#length')
  },
  weight: document.querySelector('#weight')
}

elements.type.addEventListener('change', function () {
  if (elements.type.value === 'DVD') {
    elements.dvd.removeAttribute('hidden')
  } else elements.dvd.hidden = true

  if (elements.type.value === 'furniture') {
    elements.furniture.removeAttribute('hidden')
  } else elements.furniture.hidden = true

  if (elements.type.value === 'book') {
    elements.book.removeAttribute('hidden')
  } else elements.book.hidden = true
})

window.onload = function () {
  if (elements.type.innerText === 'DVD') {
    elements.dvd.removeAttribute('hidden')
  } else elements.dvd.hidden = true
}
