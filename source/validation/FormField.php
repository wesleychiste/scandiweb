<?php

namespace Source\Validation;

class FormField
{

  protected $type;

  public function typeSwitch($post, $validation)
  {

    $this->setType($post);

    if(!isset($post)){ return $this->getDefault($validation);}
    else { switch ($this->type) {
      case 'DVD':
        return $this->getDVDError($validation);
        break;
      
      case 'book':
        return $this->getBookError($validation);
        break;

      case 'furniture':
        return $this->getFurnitureError($validation);
        break;
      
      default:
      return $this->getDefault($validation);
        break;
    } }
  }

  protected function setType($type)
  {
    $this->type = $type;
  }

  protected function getDVDError($validation)
  {
    echo "
    <div class=\"flex\">
    <div class=\"w my-2 flex justify-between\">
      <label for=\"productType\">Type Switcher</label>

      <select class=\"border my-2 rounded\" id=\"productType\" name=\"type\">
      <option selected value=\"DVD\" >DVD</option>
      </select>
    </div>
    </div>
    
    <div id=\"DVD\">
    <div class=\"flex\">
      <div class=\"w my-2 flex justify-between\">
        <label for=\"size\">Size (MB)</label>
        <input class=\"border rounded\" id=\"size\" type=\"text\"" . (!empty($_POST['size']) && !empty($validation) ? " value=" . $_POST['size'] : "") . "  name=\"size\">
      </div>
      " . (array_key_exists('size', $validation) ? "<div class=\"px-5\">" . $validation['size'] . "</div>" : "") . "
    </div>
    <small>Please provide size in MB.</small>
  </div>";
  }

  protected function getBookError($validation)
  {
    echo "
    <div class=\"flex\">
      <div class=\"w my-2 flex justify-between\">
        <label for=\"productType\">Type Switcher</label>
  
        <select class=\"border my-2 rounded\" id=\"productType\" name=\"type\">
        <option selected value=\"book\" >Book</option>
        </select>
      </div>
    </div>
          
    <div id=\"Book\">
      <div class=\"flex\">
        <div class=\"w my-2 flex justify-between\">
            <label for=\"weight\">Weight (KG)</label>
            <input class=\"border rounded\" id=\"weight\" type=\"text\"" . (!empty($_POST['weight']) && !empty($validation) ? " value=" . $_POST['weight'] : "") . " name=\"weight\">
        </div>
          " . (array_key_exists('weight', $validation) ? "<div class=\"px-5\">" . $validation['weight'] . "</div>" : "") . "
      </div>
        <small>Please provide weight in KG.</small>
    </div>";
  }

  protected function getFurnitureError($validation)
  {
    echo "
          <div class=\"flex\">
            <div class=\"w my-2 flex justify-between\">
            <label for=\"productType\">Type Switcher</label>
  
            <select class=\"border my-2 rounded\" id=\"productType\" name=\"type\">
            <option selected value=\"furniture\" >Furniture</option>
            </select>
          </div>
          </div>
          
          <div id=\"Furniture\">
        <div class=\"flex\">
          <div class=\"w my-2 flex justify-between\">
            <label for=\"height\">Height (CM)</label>
            <input class=\"border rounded\" id=\"height\" type=\"text\"" . (!empty($_POST['height']) && !empty($validation) ? " value=" . $_POST['height'] : "") . "  name=\"height\">
          </div>
          " . (array_key_exists('height', $validation) ? "<div class=\"px-5\">" . $validation['height'] . "</div>" : "") . "
        </div>
  
        <div class=\"flex\">
          <div class=\"w my-2 flex justify-between\">
            <label for=\"width\">Width (CM)</label>
            <input class=\"border rounded\" id=\"width\" type=\"text\"" . (!empty($_POST['width']) && !empty($validation) ? " value=" . $_POST['width'] : "") . " name=\"width\">
          </div>
          " . (array_key_exists('width', $validation) ? "<div class=\"px-5\">" . $validation['width'] . "</div>" : "") . "
        </div>
  
        <div class=\"flex\">
          <div class=\"w my-2 flex justify-between\">
            <label for=\"length\">Length (CM)</label>
            <input class=\"border rounded\" id=\"length\" type=\"text\"" . (!empty($_POST['length']) && !empty($validation) ? " value=" . $_POST['length'] : "") . " name=\"length\">
          </div>
          " . (array_key_exists('length', $validation) ? "<div class=\"px-5\">" . $validation['length'] . "</div>" : "") . "
        </div>
        <small>Please provide dimensions in HxWxL.</small>
      </div>";
  }

  protected function getDefault($validation)
  {
    echo "
            <div class=\"flex\">
                  <div class=\"w my-2 flex justify-between\">
                    <label for=\"productType\">Type Switcher</label>
          
                    <select class=\"border my-2 rounded\" id=\"productType\" name=\"type\">
                    <option hidden selected>Select Type</option>
                    <option value=\"DVD\">DVD</option>
                    <option value=\"book\">Book</option>
                    <option value=\"furniture\">Furniture</option>
                    </select>
                  </div>
                  " . (array_key_exists('type', $validation) ? "<div class=\"px-5\">" . $validation['type'] . "</div>" : "") . "
            </div>
            
            <div hidden id=\"DVD\">
                  <div class=\"flex\">
                    <div class=\"w my-2 flex justify-between\">
                      <label for=\"size\">Size (MB)</label>
                      <input class=\"border rounded\" id=\"size\" type=\"text\"" . (!empty($_POST['size']) && !empty($validation) ? " value=" . $_POST['size'] : "") . "  name=\"size\">
                    </div>
                    " . (array_key_exists('size', $validation) ? "<div class=\"px-5\">" . $validation['size'] . "</div>" : "") . "
                  </div>
                  <small>Please provide size in MB.</small>
                </div>
            
                <div hidden id=\"Book\">
                  <div class=\"flex\">
                    <div class=\"w my-2 flex justify-between\">
                      <label for=\"weight\">Weight (KG)</label>
                      <input class=\"border rounded\" id=\"weight\" type=\"text\"" . (!empty($_POST['weight']) && !empty($validation) ? " value=" . $_POST['weight'] : "") . " name=\"weight\">
                    </div>
                    " . (array_key_exists('weight', $validation) ? "<div class=\"px-5\">" . $validation['weight'] . "</div>" : "") . "
                  </div>
                  <small>Please provide weight in KG.</small>
                </div>
            
                <div hidden id=\"Furniture\">
                  <div class=\"flex\">
                    <div class=\"w my-2 flex justify-between\">
                      <label for=\"height\">Height (CM)</label>
                      <input class=\"border rounded\" id=\"height\" type=\"text\"" . (!empty($_POST['height']) && !empty($validation) ? " value=" . $_POST['height'] : "") . "  name=\"height\">
                    </div>
                    " . (array_key_exists('height', $validation) ? "<div class=\"px-5\">" . $validation['height'] . "</div>" : "") . "
                  </div>
            
                  <div class=\"flex\">
                    <div class=\"w my-2 flex justify-between\">
                      <label for=\"width\">Width (CM)</label>
                      <input class=\"border rounded\" id=\"width\" type=\"text\"" . (!empty($_POST['width']) && !empty($validation) ? " value=" . $_POST['width'] : "") . " name=\"width\">
                    </div>
                    " . (array_key_exists('width', $validation) ? "<div class=\"px-5\">" . $validation['width'] . "</div>" : "") . "
                  </div>
            
                  <div class=\"flex\">
                    <div class=\"w my-2 flex justify-between\">
                      <label for=\"length\">Length (CM)</label>
                      <input class=\"border rounded\" id=\"length\" type=\"text\"" . (!empty($_POST['length']) && !empty($validation) ? " value=" . $_POST['length'] : "") . " name=\"length\">
                    </div>
                    " . (array_key_exists('length', $validation) ? "<div class=\"px-5\">" . $validation['length'] . "</div>" : "") . "
                  </div>
                  <small>Please provide dimensions in HxWxL.</small>
                </div>";
  }
}