<?php

namespace Source\Validation;

use Source\Database\Crud;

require_once("source/database/Crud.php");

class InputCheck
{

  private $data;
  private $errors = [];

  public function __construct($post)
  {
    $this->data = $post;
  }

  public function validateForm()
  {

    if (isset($this->data['sku'])) {
      $this->validateSku();
    }

    if (isset($this->data['name'])) {
      $this->validateName();
    }

    if (isset($this->data['price'])) {
      $this->validatePrice();
    }

    if (isset($this->data['type'])) {
      $this->validateType();
    }

    if (isset($this->data['size'])) {
      $this->validateSize();
    }

    if (isset($this->data['height'], $this->data['width'], $this->data['length'])) {
      $this->validateDimensions();
    }

    if (isset($this->data['weight'])) {
      $this->validateWeight();
    }

    return $this->errors;
  }

  private function validateSku()
  {

    $val = trim($this->data['sku']);

    if ($val == '') {
      $this->addError('sku', 'Please, submit required data');
    } else {
      if ((new Crud)->select("sku", "sku = ", "'" . $val . "'")->fetch()) {
        $this->addError('sku', 'Sku already registered');
      }
    }
  }

  private function validateName()
  {

    $val = trim($this->data['name']);

    if ($val == '') {
      $this->addError('name', 'Please, submit required data');
    }
  }

  private function validatePrice()
  {

    $val = trim($this->data['price']);

    if ($val == '') {
      $this->addError('price', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $val)) {
        $this->addError('price', 'Field Price should have only numbers');
      }
    }
  }

  private function validateType()
  {

    $val = trim($this->data['type']);

    if ($val == 'Select Type') {
      $this->addError('type', 'Please, select a product type');
    }
  }

  private function validateSize()
  {

    $val = trim($this->data['size']);

    if ($this->data['type'] == 'DVD' && $val == '') {
      $this->addError('size', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $val)) {
        $this->addError('size', 'Field Size should have only numbers');
      }
    }
  }

  private function validateDimensions()
  {

    $height = trim($this->data['height']);
    $width = trim($this->data['width']);
    $length = trim($this->data['length']);

    if ($this->data['type'] == 'furniture' && $height == '') {
      $this->addError('height', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $height)) {
        $this->addError('height', 'Field Height should have only numbers');
      }
    }

    if ($this->data['type'] == 'furniture' && $width == '') {
      $this->addError('width', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $width)) {
        $this->addError('width', 'Field Width should have only numbers');
      }
    }

    if ($this->data['type'] == 'furniture' && $length == '') {
      $this->addError('length', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $length)) {
        $this->addError('length', 'Field Length should have only numbers');
      }
    }
  }

  private function validateWeight()
  {

    $val = trim($this->data['weight']);

    if ($this->data['type'] == 'book' && $val == '') {
      $this->addError('weight', 'Please, submit required data');
    } else {
      if (!preg_match('/^[0-9]*$/', $val)) {
        $this->addError('weight', 'Field Weight should have only numbers');
      }
    }
  }

  private function addError($key, $val)
  {
    $this->errors[$key] = $val;
  }
}
