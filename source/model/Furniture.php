<?php

namespace Source\Model;

require_once("ProductFactory.php");
require_once("source/database/Crud.php");

use Source\Model\ProductFactory;
use Source\Database\Crud;

class Furniture extends ProductFactory
{
  private $dimensions;

  private function setDimensions($dimensions)
  {
    $this->dimensions = $dimensions;
  }

  private function getDimensions()
  {
    return $this->dimensions;
  }

  public function setFurniture($sku, $name, $price, $dimensions)
  {

    parent::setProperties($sku, $name, $price);
    $this->setDimensions($dimensions);

    (new Crud)->insert(['sku' => parent::getSku(), 'name' => parent::getName(), 'price' => self::getPrice(), 'dimensions' => $this->getDimensions()]);
  }
}
