<?php

namespace Source\Model;

require_once("ProductFactory.php");
require_once("source/database/Crud.php");

use Source\Model\ProductFactory;
use Source\Database\Crud;

class DVD extends ProductFactory
{
  private $size;

  private function setSize($size)
  {
    $this->size = $size;
  }

  private function getSize()
  {
    return $this->size;
  }

  public function setDVD($sku, $name, $price, $size)
  {

    parent::setProperties($sku, $name, $price);
    $this->setSize($size);

    (new Crud)->insert(['sku' => parent::getSku(), 'name' => parent::getName(), 'price' => parent::getPrice(), 'size' => $this->getSize()]);
  }
}
