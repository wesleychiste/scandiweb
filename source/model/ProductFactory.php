<?php

namespace Source\Model;

use Source\Database\Crud;

require_once("source/database/Crud.php");

abstract class ProductFactory
{
  static $sku;
  static $name;
  static $price;

  static function setProperties($sku, $name, $price)
  {
    self::$sku = $sku;
    self::$name = $name;
    self::$price = $price;
  }

  static function getSku()
  {
    return self::$sku;
  }

  static function getName()
  {
    return self::$name;
  }

  static function getPrice()
  {
    return self::$price;
  }

  static function deleteProducts($values)
  {
    (new Crud)->delete($values);
  }

  static function getProducts($fields, $where, $value, $order, $desc)
  {
    return (new Crud)->select($fields, $where, $value, $order, $desc);
  }
}
