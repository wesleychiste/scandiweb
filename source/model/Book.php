<?php

namespace Source\Model;

require_once("ProductFactory.php");
require_once("source/database/Crud.php");

use Source\Model\ProductFactory;
use Source\Database\Crud;

class Book extends ProductFactory
{
  private $weight;

  private function setWeight($weight)
  {
    $this->weight = $weight;
  }

  private function getWeight()
  {
    return $this->weight;
  }

  public function setBook($sku, $name, $price, $weight)
  {

    parent::setProperties($sku, $name, $price);
    $this->setWeight($weight);

    (new Crud)->insert(['sku' => parent::getSku(), 'name' => parent::getName(), 'price' => self::getPrice(), 'weight' => $this->getWeight()]);
  }
}
