<?php

namespace Source\Database;

use \PDO;
use \PDOException;

class Connection
{
  private const HOST = "localhost";
  private const DBNAME = "fullstackphp";
  private const USER = "root";
  private const PASSWORD = "";

  private const OPTIONS = [
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_CASE => PDO::CASE_NATURAL
  ];

  private static $instance;

  public static function getInstance(): ?PDO
  {
    if (empty(self::$instance)) {
      try {
        self::$instance = new PDO(
          "mysql:host=" . self::HOST . ";dbname=" . self::DBNAME,
          self::USER,
          self::PASSWORD,
          self::OPTIONS
        );
      } catch (PDOException $exception) {
        echo $exception;
        die("Connection Error");
      }
    }

    return self::$instance;
  }

  private function __construct()
  {
  }

  private function __clone()
  {
  }
}
