<?php

namespace Source\Database;

use PDOException;
use Source\Database\Connection;

require_once("Connection.php");

class Crud
{

  private function execute($query, $params = [])
  {
    try {
      $statement = Connection::getInstance()->prepare($query);
      $statement->execute($params);
      return $statement;
    } catch (PDOException $e) {
      die('ERROR: ' . $e->getMessage());
    }
  }

  public function insert($values)
  {
    $fields = array_keys($values);
    $binds  = array_pad([], count($fields), '?');

    $query = 'INSERT INTO products (' . implode(',', $fields) . ') VALUES (' . implode(',', $binds) . ')';

    $this->execute($query, array_values($values));
  }

  public function select($fields = null, $where = null, $value = null, $order = null, $desc = false)
  {
    $where = strlen($where) > 0 ? 'WHERE ' . $where : '';
    $order = strlen($order) > 0 ? 'ORDER BY ' . $order : '';
    $fields = strlen($fields) > 0 ? $fields : '*';
    $value = strlen($value) > 0 ? $value : '';
    $desc = $desc ? 'DESC' : '';

    $query = 'SELECT ' . $fields . ' FROM products ' . $where . ' ' . $value . ' ' . $order . ' ' . $desc;

    return $this->execute($query);
  }

  public function delete($values)
  {
    $fields = explode(',', $values);
    $binds  = array_pad([], count($fields), '?');

    $query = 'DELETE FROM products WHERE id IN(' . implode(',', $binds) . ')';

    $this->execute($query, $fields);

    return true;
  }
}
