<?php

include("components/header.php");

require_once("source/model/ProductFactory.php");

use Source\Model\ProductFactory;

$obj = ProductFactory::getProducts('', '', '', 'id', true)->fetchAll(PDO::FETCH_OBJ);

?>

<link rel="stylesheet" href="/style/style.css">
<title>Product List</title>
</head>

<body>
  <header class="w-screen max-w-95 border-b py-5 pt-12 mx-auto">
    <div class="flex justify-between">
      <div class="text-4xl">Product List</div>
      <div class="flex justify-between">
        <div class="mr-12"><button class="text-xl bg-blue-600 py-1 px-3 text-white shadow-btn" id="add">ADD</button></div>
        <div><button id="delete-product-btn" class="text-xl bg-red-600 py-1 px-3 text-white shadow-btn">MASS DELETE</button></div>
      </div>
    </div>
  </header>
  <main class="max-w-95 mx-auto">

    <div class="grid grid-cols-4 gap-x-24 gap-y-24 my-8">

      <?php foreach ($obj as $value) {

        $number = number_format($value->price, 2, '.', ',');

        echo "<div class=\"flex flex-col items-center justify-around py-12 bg-gray-100 card border relative\">";
        echo "<input class=\"delete-checkbox absolute top-3 left-3\" type=\"checkbox\" id=\"{$value->id}\">";
        echo "<div>{$value->sku}</div>";
        echo "<div>{$value->name}</div>";
        echo "<div>{$number} $ </div>";

        if ($value->size != null) {
          echo "<div>Size: {$value->size} MB</div>";
        } elseif ($value->dimensions != null) {
          echo "<div>Dimensions: {$value->dimensions}</div>";
        } else {
          echo "<div>Weight: {$value->weight} KG</div>";
        }

        echo "</div>";
      } ?>

    </div>

    <script>
      document.querySelector("#add").addEventListener("click", function() {
        window.location = "/addproduct.php";
      });

      document.querySelector("#delete-product-btn").addEventListener("click", function() {

        let data = [];

        let id = document.querySelectorAll(".delete-checkbox");

        for (let cards of id) {
          if (cards.checked === true) {
            data.push(cards.id);
          }
        }

        let xhr = new XMLHttpRequest;
        xhr.open("POST", "delete.php", true);
        xhr.onload = function() {
          window.location = "index.php";
        };

        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send('id=' + data);

      });
    </script>

    <?php include("components/footer.php"); ?>