<?php

require_once("source/model/ProductFactory.php");
require_once("source/database/Crud.php");

use Source\Model\ProductFactory;
use Source\Database\Crud;

if (isset($_POST['id'])) {

	$values = htmlspecialchars($_POST['id']);

	ProductFactory::deleteProducts($values);
}
