<?php


require_once("source/model/DVD.php");
require_once("source/model/Furniture.php");
require_once("source/model/Book.php");

use Source\Model\DVD;
use Source\Model\Furniture;
use Source\Model\Book;

$sku = htmlspecialchars($_POST["sku"]);
$name = htmlspecialchars($_POST["name"]);
$price = htmlspecialchars($_POST["price"]);
$type = htmlspecialchars($_POST["type"]);
$size = htmlspecialchars($_POST["size"]);
$height = htmlspecialchars($_POST["height"]);
$width = htmlspecialchars($_POST["width"]);
$length = htmlspecialchars($_POST["length"]);
$dimensions = $height . "X" . $width . "X" . $length;
$weight = htmlspecialchars($_POST["weight"]);

switch ($type) {
  case "DVD":
    (new DVD)->setDVD($sku, $name, $price, $size);
    header('Location: http://localhost/index.php');
    die();
  case "furniture":
    (new Furniture)->setFurniture($sku, $name, $price, $dimensions);
    header('Location: http://localhost/index.php');
    die();
  case "book":
    (new Book)->setBook($sku, $name, $price, $weight);
    header('Location: http://localhost/index.php');
    die();
  default;
}
